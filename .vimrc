"Vim Setting file
" for Yukiya

" Vimの基本的な設定
set title 
set number
set tabstop=4
set autoindent
set shiftwidth=4

" 括弧の補完
lnoremap {<Enter> {}<Left><CR><ESC><S-o>
inoremap [<Enter> []<Left><CR><ESC><S-o>
inoremap (<Enter> ()<Left><CR><ESC><S-o>

" インサートモードでhjklによる移動
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-h> <Left>
inoremap <C-l> <Right>

" よく使用するコマンドをコロンなしで使用できるようにする
nnoremap sp :sp<CR>
nnoremap vs :vsp<CR>
nnoremap qq :q<CR>
nnoremap q! :q!<CR>
nnoremap ww :w<CR>
nnoremap wq :wq<CR>

" ウィンドウの移動をコントロールキーなしで行えるようにする
nnoremap sh <C-w>h<CR>
nnoremap sj <C-w>j<CR>
nnoremap sk <C-w>k<CR>
nnoremap sl <C-w>l<CR>

" タブ関連
nnoremap nt :tabnew<CR>
nnoremap tl gt<CR>
nnoremap th gT<CR>

" バッファ関連


" NERDTreeを呼び出したり、閉じたり
nnoremap <C-e> :NERDTreeToggle<CR>

" VimShellの呼び出し
nnoremap <C-h> :VimShell<CR>

" ヤンクした部分がクリップボードにコピーされる
set clipboard=unnamed,autoselect

" swapファイルの作成を禁止
set noswapfile

" 検索結果のハイライト
set hlsearch

" カーソル位置の行数と列を表示
set ruler

" ファイル開くときに、リスト表示
set wildmenu

if has('vim_starting')
   " 初回起動時のみruntimepathにneobundleのパスを指定する
    set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" NeoBundleを初期化
call neobundle#begin(expand('~/.vim/bundle/'))

" runtimepathにパスの追加(初回のみで良いと思う)
set runtimepath+=~.vim/autoload/vimshell.vim
set runtimepath+=~.vim/autoload/vimproc.vim
set runtimepath+=~.vim/autoload/lightline.vim
set runtimepath+=~.vim/bundle/neosnippet.vim

" 偉大なプラグインたち

" vimをIDE化
NeoBundle 'Shougo/unite.vim'

" 非同期処理するためのプラグイン
NeoBundle 'Shougo/vimproc.vim'

" ファイル操作のためのプラグイン(unite.vim必要)
NeoBundle 'Shougo/vimfiler'

" vimでシェルを使うためのプラグイン(vimproc.vim必要)
NeoBundle 'Shougo/vimshell.vim'

" ステータスラインのプラグイン
NeoBundle 'itchyny/lightline.vim'

" バッファをタブのように表示してくれるプラグイン
NeoBundle 'fholgado/minibufexpl.vim'

" なんか凄い補完してくれるプラグイン
NeoBundle 'Shougo/neocomplcache'
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'Shougo/neocomplete.vim'

" 高機能なテキスト整形プラグイン
NeoBundle 'junegunn/vim-easy-align'

" カラースキームのプラグイン
NeoBundle 'nanotech/jellybeans.vim'
NeoBundle 'w0ng/vim-hybrid'
NeoBundle 'sickill/vim-monokai'
NeoBundle 'sjl/badwolf'
NeoBundle 'vim-scripts/twilight'
NeoBundle 'djjcast/mirodark'
NeoBundle 'itchyny/landscape.vim'
NeoBundle 'tomasr/molokai'
NeoBundle 'cocopon/iceberg.vim'

" ツリー状に表示
NeoBundle 'scrooloose/nerdtree'

" かっこいい起動画面！
NeoBundle 'thinca/vim-splash'

"bufexplの設定
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBuffs = 1
nnoremap bl :bnext <CR>
nnoremap bh :bprev <CR>

" snippetの設定
highlight Pmenu ctermbg=4
highlight PmenuSel ctermbg=1
highlight PMenuSbar ctermbg=4

" 補完ウィンドウの設定
set completeopt=menuone

" 補完ウィンドウの設定
set completeopt=menuone

" rsenseでの自動補完機能を有効化
let g:rsenseUseOmniFunc = 1
" let g:rsenseHome = '/usr/local/lib/rsense-0.3'

" auto-ctagsを使ってファイル保存時にtagsファイルを更新
let g:auto_ctags = 1

" 起動時に有効化
let g:neocomplcache_enable_at_startup = 1

" 大文字が入力されるまで大文字小文字の区別を無視する
let g:neocomplcache_enable_smart_case = 1

" _(アンダースコア)区切りの補完を有効化
let g:neocomplcache_enable_underbar_completion = 1

let g:neocomplcache_enable_camel_case_completion  =  1

" 最初の補完候補を選択状態にする
let g:neocomplcache_enable_auto_select = 1

" ポップアップメニューで表示される候補の数
let g:neocomplcache_max_list = 20

" シンタックスをキャッシュするときの最小文字長
let g:neocomplcache_min_syntax_length = 3

" 補完の設定
autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
if !exists('g:neocomplete#force_omni_input_patterns')
  let g:neocomplete#force_omni_input_patterns = {}
endif
let g:neocomplete#force_omni_input_patterns.ruby = '[^.*\t]\.\w*\|\h\w*::'

if !exists('g:neocomplete#keyword_patterns')
        let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" LightLineの有効化
let g:lightline = {
        \ 'colorscheme': 'landscape',
        \ 'mode_map': {'c': 'NORMAL'},
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ], [ 'fugitive', 'filename' ] ]
        \ },
        \ 'component_function': {
        \   'modified': 'LightLineModified',
        \   'readonly': 'LightLineReadonly',
        \   'fugitive': 'LightLineFugitive',
        \   'filename': 'LightLineFilename',
        \   'fileformat': 'LightLineFileformat',
        \   'filetype': 'LightLineFiletype',
        \   'fileencoding': 'LightLineFileencoding',
		\   'mode': 'LightLineMode'
		\ }
		\ }

function! LightLineModified()
  return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! LightLineReadonly()
  return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? 'x' : ''
endfunction

function! LightLineFilename()
  return ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
        \ (&ft == 'vimfiler' ? vimfiler#get_status_string() :
        \  &ft == 'unite' ? unite#get_status_string() :
        \  &ft == 'vimshell' ? vimshell#get_status_string() :
        \ '' != expand('%:t') ? expand('%:t') : '[No Name]') .
        \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
endfunction

function! LightLineFugitive()
  try
    if &ft !~? 'vimfiler\|gundo' && exists('*fugitive#head')
      return fugitive#head()
    endif
  catch
  endtry
  return ''
endfunction

function! LightLineFileformat()
  return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! LightLineFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype : 'no ft') : ''
endfunction

function! LightLineFileencoding()
  return winwidth(0) > 70 ? (strlen(&fenc) ? &fenc : &enc) : ''
endfunction

function! LightLineMode()
  return winwidth(0) > 60 ? lightline#mode() : ''
endfunction

call neobundle#end()
syntax enable
" カラースキームを有効
colorscheme molokai
" 257色を表示
set t_Co=256

" ファイルタイプ別のプラグイン/インデントを有効にする
filetype plugin indent on
NeoBundleCheck
